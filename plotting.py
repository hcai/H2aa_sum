import ROOT
import array
from inspect import currentframe, getframeinfo
frameinfo = getframeinfo(currentframe())


#from ROOT import TGraph
#from ROOT import TMultiGraph

ROOT.gROOT.ProcessLine('.L AtlasStyle.C+')
ROOT.SetAtlasStyle()
ROOT.gStyle.SetTitleOffset(1.2)
ROOT.gStyle.SetLineWidth(2)

c1 = ROOT.TCanvas("c1","c1",800,800)
c1.SetLogx()
c1.SetLogy()

#----read branching ratio----

ggF_xsec = 48.58 

channelList = []
channelList.append('bb')
channelList.append('mumu')
channelList.append('tautau')
channelList.append('gammagamma')
channelList.append('gg')
channelList.append('jj')
channelList.append('jjbc')

typeList = []
typeList.append('Type_I')
typeList.append('Type_II_beta0.5')
typeList.append('Type_II_beta0.5alpha0.1')
typeList.append('Type_II_beta5')
typeList.append('Type_III_beta0.5')
typeList.append('Type_III_beta0.5alpha-1.4')
typeList.append('Type_III_beta5')
typeList.append('Type_IV_beta0.5')
typeList.append('Type_IV_beta0.5alpha0.1')
typeList.append('Type_IV_beta5')

texTypeList = {'Type_I':                    '2HDM+S Type-I',
               'Type_II_beta0.5':           '2HDM+S Type-II, tan#beta = 0.5',
               'Type_II_beta0.5alpha0.1':   '2HDM+S Type-II, tan#beta = 0.5, #alpha = 0.1',
               'Type_II_beta5':             '2HDM+S Type-II, tan#beta = 5',
               'Type_III_beta0.5':          '2HDM+S Type-III, tan#beta = 0.5',
               'Type_III_beta0.5alpha-1.4': '2HDM+S Type-III, tan#beta = 0.5, #alpha = -1.4',
               'Type_III_beta5':            '2HDM+S Type-III, tan#beta = 5',
               'Type_IV_beta0.5':           '2HDM+S Type-IV, tan#beta = 0.5',
               'Type_IV_beta0.5alpha0.1':   '2HDM+S Type-IV, tan#beta = 0.5, #alpha = 0.1',
               'Type_IV_beta5':             '2HDM+S Type-IV, tan#beta = 5'}

branchingRatio = {}
for which_type in typeList:
    channels = {}
    for which_channel in channelList:
        channels[which_channel] = ROOT.TGraph('branchingRatio/%s/%s.txt' % (which_type,which_channel))
    branchingRatio[which_type] = channels

#----read analysis results----

bbbbTFile = ROOT.TFile("data/limits_BR_bbbb.root")
bbmumuTFile = ROOT.TFile("data/Limits_Hbbmm.root")

def read4mu_entries(index, flag):

    if flag == "l":
        f = open('data/limits_4mu_lowMass.txt','r')
    if flag == "h":
        f = open('data/limits_4mu_highMass.txt','r')
    x = []
    y = []
    for columns in ( raw.strip().split() for raw in f ):
        x.append(float(columns[0]))
        y.append(float(columns[index])*1e-3/ggF_xsec)  
    xx = array.array('d',x)
    yy = array.array('d',y)
    #print xx
    #print yy
    graph = ROOT.TGraph(len(xx), xx, yy)
    return graph

analysis_nominal = {'mumutautau':   ROOT.TGraph("data/limits_mumutautau_expected.txt"),
                    'bbbb':         bbbbTFile.gExpLimit,
                    'bbmumu':       bbmumuTFile.Nominal,
                    #'mumumumu':     ROOT.TGraph("data/limits_4mu_expected.txt"),
                    'mumumumu':     read4mu_entries(2,"l"),
                    'mumumumu2':    read4mu_entries(2,"h"),
                    '4gamma':       ROOT.TGraph("data/limits_4gammas_expected.txt"),
                    'gammagammagg': ROOT.TGraph("data/limits_gammagammagg_expected.txt")}

analysis_up1sig = {'bbbb':          bbbbTFile.gExp1SUp,
                   'bbmumu':        bbmumuTFile.up1sigma,
                   #'mumumumu':      ROOT.TGraph('data/limits_4mu_expected_1up.txt'),
                   'mumumumu':      read4mu_entries(4,"l"),
                   'mumumumu2':     read4mu_entries(4,"h"),
                   '4gamma':        ROOT.TGraph('data/limits_4gammas_expected_1up.txt'),
                   'gammagammagg':  ROOT.TGraph('data/limits_gammagammagg_expected_1up.txt')}

analysis_dw1sig = {'bbbb':          bbbbTFile.gExp1SDw,
                   'bbmumu':        bbmumuTFile.down1sigma,
                   #'mumumumu':      ROOT.TGraph('data/limits_4mu_expected_1dw.txt'),
                   'mumumumu':      read4mu_entries(3,"l"),
                   'mumumumu2':     read4mu_entries(3,"h"),
                   '4gamma':        ROOT.TGraph('data/limits_4gammas_expected_1dw.txt'),
                   'gammagammagg':  ROOT.TGraph('data/limits_gammagammagg_expected_1dw.txt')}

analysis_up2sig = {'bbbb':          bbbbTFile.gExp2SUp,
                   'bbmumu':        bbmumuTFile.up2sigma}

analysis_dw2sig = {'bbbb':          bbbbTFile.gExp2SUp,
                   'bbmumu':        bbmumuTFile.down2sigma}

analysis_obs = {'bbbb':          bbbbTFile.gObsLimit,
                #'mumumumu':      ROOT.TGraph('data/limits_4mu_observed.txt'),
                'bbmumu':        bbmumuTFile.Observed,
                'mumumumu':      read4mu_entries(1,"l"),
                'mumumumu2':     read4mu_entries(1,"h"),
                '4gamma':        ROOT.TGraph('data/limits_4gammas_observed.txt'),
                'gammagammagg':  ROOT.TGraph('data/limits_gammagammagg_observed.txt')}


analysisTitle =  {'bbbb':        "h#rightarrow aa#rightarrow bbbb (ATL-COM-PHYS-2017-086)",
                  'mumutautau':  "h#rightarrow aa#rightarrow #mu#mu#tau#tau (PRD 92 052002(2015))",
                  'bbmumu':      "h#rightarrow aa#rightarrow bb#mu#mu (ATL-COM-PHYS-2015-1553)",
                  'mumumumu':    "h#rightarrow aa#rightarrow #mu#mu#mu#mu (ATL-COM-PHYS-2017-460)",
                  '4gamma':      "h#rightarrow aa#rightarrow #gamma#gamma#gamma#gamma (EPJC 76(4) 1-26(2016))",
                  'gammagammagg':"h#rightarrow aa#rightarrow #gamma#gammajj (ATL-COM-PHYS-2017-376)"}

analysisList = []
analysisList.append('bbbb')
analysisList.append('mumutautau')
analysisList.append('bbmumu')
analysisList.append('mumumumu')
analysisList.append('4gamma')
analysisList.append('gammagammagg')

def removeOddPoint(thisGraph):
    x = array.array('d',[0])
    y = array.array('d',[0])
    thisGraph.GetPoint(1,x,y)
    thisGraph.SetPoint(0,x[0],y[0])
    return thisGraph

analysis_nominal['bbbb'] = removeOddPoint(analysis_nominal['bbbb'])
analysis_up1sig['bbbb'] = removeOddPoint(analysis_up1sig['bbbb'])
analysis_dw1sig['bbbb'] = removeOddPoint(analysis_dw1sig['bbbb'])
analysis_up2sig['bbbb'] = removeOddPoint(analysis_up2sig['bbbb'])
analysis_dw2sig['bbbb'] = removeOddPoint(analysis_dw2sig['bbbb'])
analysis_obs['bbbb'] = removeOddPoint(analysis_obs['bbbb'])

ch1={"mumutautau":"tautau", "bbbb":"bb", "mumumumu":"mumu", "mumumumu2":"mumu",
     "bbmumu":"bb",   "4gamma":"gammagamma", "gammagammagg":"gammagamma"}
ch2={"mumutautau":"tautau", "bbbb":"bb", "mumumumu":"mumu", "mumumumu2":"mumu",
     "bbmumu":"mumu", "4gamma":"gammagamma", "gammagammagg":"jjbc"}

#results_nominal = {'mumutautau': CalcTGraph(analysis_nominal['mumutautau'],branchingRatio[thisType]["bb"])}

#-----------------------------------------------
#-----START OF PLOTTING VARIABLE DEFINITION-----
#-----------------------------------------------

jpsi_left = 3.
jpsi_right = 5.01
upsi_left = 8.5
upsi_right = 11.5


xAxisTitleOffset = 1.1
xAxisTitleSize = 0.05
xAxisLabelSize = 0.03

yAxisTitleOffset = 1.3
yAxisTitleSize = 0.04
yAxisLabelSize = 0.03

yAxisMin = {'Type_I':                    1e-8,
            'Type_II_beta0.5':           1e-6,
            'Type_II_beta0.5alpha0.1':   1e-6,
            'Type_II_beta5':             1e-7,
            'Type_III_beta0.5':          3e-7,
            'Type_III_beta0.5alpha-1.4': 1e-7,
            'Type_III_beta5':            1e-10,
            'Type_IV_beta0.5':           1e-6,
            'Type_IV_beta0.5alpha0.1':   1e-7,
            'Type_IV_beta5':             4e-7}

yAxisMax = {'Type_I':                    1e6,
            'Type_II_beta0.5':           1e5,
            'Type_II_beta0.5alpha0.1':   1e6,
            'Type_II_beta5':             1e6,
            'Type_III_beta0.5alpha-1.4': 1e6,
            'Type_III_beta0.5':          1e6,
            'Type_III_beta5':            1e7,
            'Type_IV_beta0.5':           1e5,
            'Type_IV_beta0.5alpha0.1':   1e6,
            'Type_IV_beta5':             1e7}


xAxisMin = 0.5
xAxisMax = 60

graphAlpha = 0.6
graphColors = {"mumutautau":ROOT.kPink, 
               "bbbb":ROOT.kBlue, 
               "mumumumu":ROOT.kGreen,
               "mumumumu2":ROOT.kGreen,
               "bbmumu":ROOT.kMagenta, 
               "4gamma":ROOT.kOrange,
               "gammagammagg":ROOT.kTeal}

#--------------------------------------------
#-----END OF PLOTTING VARIABLE DEFINITION----
#--------------------------------------------

def CalcMiddleValue(x1,y1,x2,y2,x_val):
    k = (y2-y1)/(x2-x1)
    y_val = k * (x_val-x1) + y1
    return y_val
    
def FindDenominator(br_graph, x):
    
    N = br_graph.GetN()

    for i in range(N-1):
        
        x_1 = array.array('d',[0])
        x_2 = array.array('d',[0])
        y_1 = array.array('d',[0])
        y_2 = array.array('d',[0])

        br_graph.GetPoint(i,  x_1, y_1)
        br_graph.GetPoint(i+1,x_2, y_2)

        #case of find exact point
        if x_1[0]==x:
            return y_1[0]
        #case of find proper interval
        if x_1[0]<x and x_2[0]>x:
            y = CalcMiddleValue(x_1[0],y_1[0],x_2[0],y_2[0],x)
            return y

    return -1
        
def ModifyTGraph(origin_graph):
  
    ymax_forced = 1e20
    
    N = origin_graph.GetN()

    xmin = array.array('d',[0])
    xmax = array.array('d',[0])
    y_dummy = array.array('d',[0])
    origin_graph.GetPoint(0,xmin,y_dummy)
    origin_graph.GetPoint(N-1,xmax,y_dummy)
    
    new_graph = origin_graph.Clone()
    new_graph.SetPoint(new_graph.GetN(), xmax[0], ymax_forced)
    new_graph.SetPoint(new_graph.GetN(), xmin[0], ymax_forced)
    #print xmin[0], xmax[0]
    return new_graph

def ModifyByLimits(graph_up,graph_dw):
    
    N = graph_up.GetN()
      
    new_graph = graph_up.Clone()
    for i in range(N):
        x = array.array('d',[0])
        y = array.array('d',[0])
        graph_dw.GetPoint(N-1-i, x, y)
        new_graph.SetPoint(new_graph.GetN(),x[0],y[0])
    return new_graph

        
def CalcTGraph(numerator_graph,denominator_graph1, denominator_graph2):

    N = numerator_graph.GetN()
    new_graph = ROOT.TGraph()
    for  i in range(N):
        numerator_x = array.array('d',[0])
        numerator_y = array.array('d',[0])
        numerator_graph.GetPoint(i, numerator_x, numerator_y)

        denominator1 = FindDenominator(denominator_graph1, numerator_x[0])
        if denominator1==0:
            denominator1=1e-9
        denominator2 = FindDenominator(denominator_graph2, numerator_x[0])
        if denominator2==0:
            denominator2=1e-9
        if denominator1==-1 or denominator2==-1:
            continue
        #print numerator_x[0], denominator1, denominator2
        new_graph.SetPoint(i, numerator_x[0], numerator_y[0]/(denominator1*denominator2))

    return new_graph

#name of anlaysis, type of theory, name of TGraph collection, if have up&down of 1 sigma, if have up%down uncertainty of 2 sigma, if have obs 
#example: "mumumumu", mg, thisType, true, true, false
def GetBranchingRatio(which_anal, mg, thisType, if_1sigma, if_2sigma, if_obs):

    #print which_anal, ch1[which_anal], ch2[which_anal]
    results_nominal = CalcTGraph(analysis_nominal[which_anal],
                                 branchingRatio[thisType][ch1[which_anal]],
                                 branchingRatio[thisType][ch2[which_anal]])
    results_nominal.SetFillColorAlpha(graphColors[which_anal], graphAlpha)
    results_nominal.SetFillStyle(3002)
    mg.Add(ModifyTGraph(results_nominal),"F")
    #print analysisTitle[which_anal]

    results_exp = CalcTGraph(analysis_nominal[which_anal],
                             branchingRatio[thisType][ch1[which_anal]],    
                             branchingRatio[thisType][ch2[which_anal]])
    results_exp.SetLineColor(graphColors[which_anal])
    results_exp.SetLineStyle(7)
    results_exp.SetLineWidth(2)
    mg.Add(results_exp,"C")

    if if_1sigma:
        results_up1sig = CalcTGraph(analysis_up1sig[which_anal],
                                    branchingRatio[thisType][ch1[which_anal]],
                                    branchingRatio[thisType][ch2[which_anal]])
        results_dw1sig = CalcTGraph(analysis_dw1sig[which_anal],
                                    branchingRatio[thisType][ch1[which_anal]],
                                    branchingRatio[thisType][ch2[which_anal]])
        results_1sig = ModifyByLimits(results_up1sig, results_dw1sig)
        results_1sig.SetFillColor(graphColors[which_anal])
        results_1sig.SetFillStyle(3005)
        mg.Add(results_1sig,"F")

    if if_obs:
        results_obs = CalcTGraph(analysis_obs[which_anal],
                                 branchingRatio[thisType][ch1[which_anal]],
                                 branchingRatio[thisType][ch2[which_anal]])
        results_obs.SetLineColor(graphColors[which_anal])
        results_obs.SetLineWidth(3)
        mg.Add(results_obs,"C")    

    return mg


def replaceScaler(_string, _flag):
    if _flag==False:
        return _string
    else:
        return _string.replace("#rightarrow aa", "#rightarrow ss")
    

def makeTestPlot(thisType, leg, isScaler):
    
    c1.Clear()
    c1.cd()

   
    mg = ROOT.TMultiGraph()  
    TLine = ROOT.TGraph()
    TLine.SetPoint(0,0., 1.)
    TLine.SetPoint(1,xAxisMax, 1.)
    TLine.SetLineColor(ROOT.kRed)
    TLine.SetLineWidth(5)
    TLine.SetLineStyle(9)

    x_jpsi = array.array('d',[jpsi_left, jpsi_right, jpsi_right, jpsi_left])
    y_jpsi = array.array('d',[yAxisMin[thisType],  yAxisMin[thisType],   yAxisMax[thisType],   yAxisMax[thisType]])
    block_jpsi = ROOT.TGraph(len(x_jpsi), x_jpsi, y_jpsi)
    block_jpsi.SetFillColor(ROOT.kGray)
    block_jpsi.SetLineWidth(0)
    
    x_upsi = array.array('d',[upsi_left, upsi_right, upsi_right, upsi_left])
    block_upsi = ROOT.TGraph(len(x_upsi), x_upsi, y_jpsi)
    block_upsi.SetFillColor(ROOT.kGray)    

    x_below1 = array.array('d',[0., 1.00, 1.00, 0.])
    block_x_below1 = ROOT.TGraph(len(x_below1), x_below1, y_jpsi)
    block_x_below1.SetFillColor(ROOT.kGray)
    
    mg = GetBranchingRatio("mumutautau",   mg, thisType, False, False, False)
    mg = GetBranchingRatio("bbbb",         mg, thisType, True,  False, False)
    mg = GetBranchingRatio("bbmumu",       mg, thisType, True,  False, True)
    mg = GetBranchingRatio("mumumumu",     mg, thisType, True,  False, True)
    mg = GetBranchingRatio("mumumumu2",    mg, thisType, True,  False, True)
    mg = GetBranchingRatio("gammagammagg", mg, thisType, True,  False, True)
    mg = GetBranchingRatio("4gamma",       mg, thisType, False, False, True)
 
    mg.Add(block_jpsi,     "F")
    mg.Add(block_upsi,     "F")
    mg.Add(block_x_below1, "F")
    mg.Add(TLine,"L")    

    mg.Draw("A")

    mg.GetXaxis().SetTitle("m_{a} [GeV]")
    mg.GetXaxis().SetTitleSize(xAxisTitleSize)
    mg.GetXaxis().SetTitleOffset(xAxisTitleOffset)
    mg.GetXaxis().SetLabelSize(xAxisLabelSize)
    mg.GetXaxis().SetRangeUser(xAxisMin, xAxisMax)

    mg.GetYaxis().SetTitle(replaceScaler("95% CL on #frac{#sigma(H)}{#sigma_{SM}}#times B(H#rightarrow aa)",isScaler))
    mg.GetYaxis().SetTitleSize(yAxisTitleSize)
    mg.GetYaxis().SetTitleOffset(yAxisTitleOffset)
    mg.GetYaxis().SetRangeUser(yAxisMin[thisType], yAxisMax[thisType])
    mg.GetYaxis().SetLabelSize(yAxisLabelSize)

    texType = ROOT.TLatex()
    texType.SetNDC()
    texType.SetTextSize(0.025)
    texType.SetTextFont(32)
    texType.DrawLatex(0.47, 0.47, leg)

    texATLAS = ROOT.TLatex()
    texATLAS.SetNDC(ROOT.kTRUE)
    texATLAS.SetTextSize(0.035)
    texATLAS.SetTextFont(72)
    texATLAS.DrawLatex(0.2, 0.9, "ATLAS Internal")


    c1.RedrawAxis()
    lg = c1.BuildLegend(0.47, 0.18, 0.95, 0.45)
    lg.Clear()
    lg.SetTextFont(82)
    lg.SetFillColor(0)
    lg.SetBorderSize(0)
    #lg.SetFillStyle(0)
    #lg.AddEntry(TLine, leg, "")
    #lg.AddEntry(TLine, "h#rightarrow aa analysis", "")    
    dummy_graph = {}
    lg.AddEntry(block_jpsi, "No prediction for Br(a#rightarrowXX)","FL")
    for thisAnalysis in analysisList:
        dummy_graph[thisAnalysis] = ROOT.TGraph()
        dummy_graph[thisAnalysis].SetPoint(0, xAxisMin, 1.)
        dummy_graph[thisAnalysis].SetPoint(1, xAxisMax, 1.)
        dummy_graph[thisAnalysis].SetPoint(2, 0., 0.)
        dummy_graph[thisAnalysis].SetFillColorAlpha(graphColors[thisAnalysis],graphAlpha)
        dummy_graph[thisAnalysis].SetFillStyle(3002)
        dummy_graph[thisAnalysis].SetLineColor(graphColors[thisAnalysis])
        lg.AddEntry(dummy_graph[thisAnalysis], replaceScaler(analysisTitle[thisAnalysis],isScaler), "FL")
        
        
    dummy_graph1 = ROOT.TGraph()
    dummy_graph1.SetPoint(0,xAxisMin,1.)
    dummy_graph1.SetPoint(1,xAxisMax,1.)
    dummy_graph1.SetPoint(2,0.,0.)
    dummy_graph1.SetFillColor(ROOT.kGray)
    dummy_graph1.SetFillStyle(3005)
    dummy_graph1.SetLineColor(ROOT.kGray+3)
    dummy_graph1.SetLineStyle(7)
    dummy_graph1.SetLineWidth(2)

    dummy_graph2 = ROOT.TGraph()
    dummy_graph2.SetPoint(0,xAxisMin,1.)
    dummy_graph2.SetPoint(1,xAxisMax,1.)
    dummy_graph2.SetPoint(2,0.,0.)
    dummy_graph2.SetFillColor(ROOT.kGray)
    dummy_graph2.SetFillStyle(3002)
    dummy_graph2.SetLineColor(ROOT.kGray+3)
    dummy_graph2.SetLineWidth(2)

    lg2 = c1.BuildLegend(0.3, 0.18, 0.47, 0.2571)    
    lg2.Clear()
    lg2.SetTextFont(82)
    lg2.SetFillColor(0)
    lg2.SetBorderSize(0)
    lg2.AddEntry(dummy_graph1,"expected","FL")
    lg2.AddEntry(dummy_graph2,"observed","FL")    
    
    c1.Modified()
    c1.Print('figures/%s.pdf' % thisType)

def makePlots():

    for which_type in typeList:
        if texTypeList[which_type].find('alpha')==-1:
            makeTestPlot(which_type,texTypeList[which_type], False)
        else:
            makeTestPlot(which_type,texTypeList[which_type], True)
     
if __name__ == "__main__":
    makePlots()
